import webpack from 'webpack'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
// import HashPlugin from 'hash-webpack-plugin'
import debug from 'debug'

import {
  resolve
} from 'path'
import {
  get
} from 'http'

const dbg = debug('ddd-assets')

const publicPath = resolve(__dirname, 'dist/public')

// optimization, see: https://github.com/webpack-contrib/mini-css-extract-plugin
const compiler = webpack(
  {
    mode: 'development',
    entry: [
      resolve(__dirname, 'src/public/js/main.js'),
      resolve(__dirname, 'src/public/js/globals.js')
    ],
    output: {
      path: publicPath,
      filename: '[name].js',
      library: 'dotdotdown',
      libraryTarget: 'window'
    },
    resolve: {
      extensions: ['.js', '.less', '.pug']
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].[hash].css'
      })
      // new HashPlugin({ path: publicPath, fileName: 'hash.txt' })
      // new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery',
      //   'window.jQuery': 'jquery'
      // })
    ],
    module: {
      rules: [
        // use expose-loader instead of webpack.ProvidePlugin
        // this allows external components to use the same instance of jquery
        // which gives them access to $().component() defined in Component
        {
          test: require.resolve('jquery'),
          use: [{
            loader: 'expose-loader',
            options: 'jQuery' // paste.js expects `window.jQuery`
          },{
            loader: 'expose-loader',
            options: '$'
          }]
        },
        {
          test: /\.(c|le)ss$/,
          use: [
            { loader: MiniCssExtractPlugin.loader },
            {
              loader: 'css-loader',
              options: {
                modules: false // better to specify what you want locally scoped
              }
            },
            { loader: 'less-loader' } // compiles Less to CSS
          ]
        },
        {
          test: /\.pug/,
          use: [
            { loader: 'pug-loader' }
          ]
        }
      ]
    }
  }
)

compiler.run((err, stats) => {
  if (err) dbg(err)
  else if (stats.hasErrors()) dbg(stats.compilation.errors)
  // trigger browsersync reload
  else {
    dbg(`webpack looks ok: ${stats.hash}`)
  }
  if (process.env.NODE_ENV === 'dev') {
    dbg('assets (re)built, request browser reload')
    get('http://localhost:3001/__browser_sync__?method=reload')
    .on('error', () => console.log('browser-sync not listening'))
  }
})
