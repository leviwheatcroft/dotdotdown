## dotdotdown

DotDotDown is a browser based markdown editor for local file systems with a focus on image management.

*features*

 * rad markdown editor. codemirror get's the credit for this.
 * pasted image handling. select some text and paste an image, that's it.
 * cool vanilla js web components.

*what it's not*

 * It's not a wiki engine. Those have history, user management, and a linking strategy.
 * It's not a file manager. You can navigate the file system and open markdown files, but other functions are best left to a proper file manager.
 * It's not a remote file editor. You could access the ui over a network, but none of the relevant security concerns have been considered.

 ## installation

*prerequisites*

 * yarn
 * node > 10.2.0

 ```
 git clone git@gitlab.com:leviwheatcroft/dotdotdown.git
 yarn
 yarn run build
 DDD_ROOT="~\path-to-root" yarn start
 ```

You can edit `config\default.js` to set a root directory, and thereafter start with just `yarn start`
