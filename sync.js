import browserSync from 'browser-sync'

browserSync.create()
.init({
  open: false,
  proxy: 'localhost:3000',
  // files: [
  //   'src/**/*',
  //   '!src/public'
  // ],
  reloadDelay: 1000,
  port: 3001
})
