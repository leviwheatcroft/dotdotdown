module.exports = {
  root: 'data',
  path: {
    public: './dist/public'
  },
  plugins: [
    'dotdotdown-create-file',
    'dotdotdown-image-paste',
    'dotdotdown-save'
  ]
}
