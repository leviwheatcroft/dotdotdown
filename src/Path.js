import {
  escapeRegExp
} from 'lodash'
import {
  join,
  sep,
  parse,
  normalize,
  relative
} from 'path'
import debug from 'debug'
import config from 'config'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-path')

export default class Path {
  constructor (path) {
    this.path = path
  }
  asURI () {
    let uri = this.path
    // remove root path
    const re = new RegExp('^' + escapeRegExp(config.get('root')))
    if (re.test(uri)) {
      uri = uri.replace(re, '')
      // ensure it starts with a sep
      uri = join(sep, uri)
    }
    // replace all windows sep with /
    uri = uri.replace(/\\/g, '/')
    // percent encode
    uri = encodeURI(uri)
    return uri
  }
  parent () {
    if (relative(this.path, config.get('root')).length === 0) return false
    return new Path(this.parse().dir)
  }
  ancestors () {
    const ancestors = []
    let ancestor = this
    // eslint-disable-next-line no-cond-assign
    while (ancestor = ancestor.parent()) ancestors.unshift(ancestor)
    return ancestors
  }
  child (path) {
    return new Path(join(this.path, path))
  }
  relativeTo(path) {
    this.path = relative(path, this.path)
    return this
  }
  asPath () {
    return this.path
  }
  parse () {
    return parse(this.path)
  }
  increment () {
    const re = /-(\d{3})\.[A-Za-z]{3,4}$/
    const match = this.path.match(re)
    let current
    if (!match) current = 0
    else current = parseInt(match[1])
    const next = (current + 1).toString().padStart(3, '0')
    const ext = this.parse().ext
    const pop = current ? ext.length + 4 : ext.length
    const slice = this.path.slice(0, pop * -1)
    this.path = `${slice}-${next}${ext}`
    return this
  }
  static from (path) {
    // deal with percent escaped stuff
    path = decodeURI(path)
    // switch separators to / or \
    path = normalize(path)
    // add root path if missing
    const re = new RegExp('^' + escapeRegExp(config.get('root')))
    if (!re.test(path)) path = join(config.get('root'), path)
    return new Path(path)
  }
}
