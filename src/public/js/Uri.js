/* eslint-env browser */

export default class Uri {
  constructor (uri = window.location.pathname) {
    this.uri = uri
    // get rid of these
    this.dir = this.getDir() // deprecated
    this.base = this.getBase() // deprecated
    this.ext = this.getExt() // deprecated
  }
  getUri () {
    return this.uri
  }
  getUriHuman () {
    return decodeURIComponent(this.uri)
  }
  getDir () {
    let uri = this.uri.split(/\//)
    // ditch last entry, will be empty string if pathname is directory
    uri.pop()
    // empty string on the end means join will terminate string with join char
    uri.push('')
    uri = uri.join('/')
    return uri
  }
  getDirHuman () {
    return decodeURIComponent(this.getDir())
  }
  getBase () {
    let match = /[^/]*$/.exec(this.uri)
    if (!match) return false
    return match[0]
  }
  getBaseHuman () {
    return decodeURIComponent(this.getBase())
  }
  setBase (base) {
    this.uri = `${this.getDir()}${base}`
    return this.getUri()
  }
  setBaseHuman (base) {
    return decodeURIComponent(this.setBase(encodeURIComponent(base)))
  }
  getExt () {
    let match = /[^./]*$/.exec(this.uri)
    if (!match) return false
    return match[0]
  }
  isDir () {
    return /\/$/.test(this.uri)
  }
  relative (to = window.location.pathname) {
    to = new Uri(to)
    // so dirty
    return this.uri.replace(to.dir, '')
  }
}
