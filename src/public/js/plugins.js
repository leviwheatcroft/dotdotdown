/* eslint-env browser */
/* global */

const plugins = []

export function registerPlugin (plugin) {
  plugins.push(plugin)
}
export function loadPlugins () {
  plugins.forEach((plugin) => plugin.load())
}
