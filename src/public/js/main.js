/* eslint-env browser */
/* global $ */
import 'jquery'
import '../stylesheets/style.less'
import 'popper.js'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'

import Editor from './components/Editor'
import Nav from './components/Nav'
import Alert from './components/Alert'
// import ImagePaste from './components/ImagePaste'
import Dir from './components/Dir'
// import Create from './components/Create'
// import Save from './components/Save'
import Directory from './components/Directory'
// import Unsplash from './components/Unsplash'
import {
  registerPlugin,
  loadPlugins
} from './plugins'

registerPlugin(Nav)
registerPlugin(Alert)
registerPlugin(Editor)
// registerPlugin(ImagePaste)
registerPlugin(Dir)
// registerPlugin(Save)
registerPlugin(Directory)

$('document').ready(() => loadPlugins())

// Nav.load()
// Alert.load($('.master-alert'))
// Editor.load()
// ImagePaste.load()
// Dir.load()
// // Create.load()
// Save.load()
// Directory.load()
