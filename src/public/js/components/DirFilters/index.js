/* eslint-env browser */
/* global $ */

import styles from './styles'
import layout from './layout'
import Component from '../Component'
// import pick from 'lodash'

export default class DirFilters extends Component {
  constructor () {
    super(layout, styles)
    this.resetFilters()
  }
  render () {
    super.render(this.filters)
    return this
  }
  resetFilters () {
    this.filters = {
      fileName: '',
      markdown: true,
      directory: true,
      image: true
    }
  }
  clickClear () {
    this.resetFilters()
    this.applyFilter()
    this.render()
  }
  clickSubmit (event) {
    event.preventDefault()
    // it's not as easy as getting a simple object
    // if checkboxes are unchecked, they won't be included in formData.entries()
    const formData = new FormData($('form', this.el)[0])
    Object.assign(this.filters, {
      fileName: formData.get('fileName'),
      markdown: formData.get('markdown'),
      directory: formData.get('directory'),
      image: formData.get('image')
    })
    this.applyFilter()
  }
  applyFilter () {
    const {
      fileName,
      markdown,
      directory,
      image
    } = this.filters
    console.log(markdown)
    const fileNameRE = new RegExp(fileName, 'i')
    $('#dir').component().applyFilter((nodes) => {
      return nodes.filter((node) => {
        if (!fileNameRE.test(node.name)) return
        if (!markdown && node.isMd) return
        if (!directory && node.isDirectory) return
        if (!image && node.isImage) return
        return true
      })
    })
  }
  static async load () {
    const dirFilters = new DirFilters()
    $('#dir-filters').replaceWith(dirFilters.render().el)
  }
}

