/* eslint-env browser */
/* global $ */
import styles from './styles'
import layout from './layout'
import alertLayout from './alertLayout'
import Component from '../Component'
import classNames from 'classnames'

export default class Alert extends Component {
  constructor () {
    super(layout, styles)
  }
  // render () {
  //   super.render()
  // }
  /**
   * see ./alertLayout for locals
   * locals.context - bootstrap context like 'primary' or 'success'
   *   see: http://getbootstrap.com/docs/4.0/components/alerts/
   * locals.content - (String) text content
   * locals.timeout - (Int) auto dispose in `timeout` ms
   * locals.dismissable - (Boolean) show close button
   */
  addAlert (locals) {
    Object.assign(locals, { classNames })
    const alert = $(alertLayout(locals))
    this.el.append(alert)
    if (locals.timeout) {
      setTimeout(() => { alert.alert('close') }, locals.timeout)
    }
  }
  static load (parent = '.master-alert') {
    parent = $(parent)
    const parentClasses = parent.attr('class').split(' ')
    parent.replaceWith(new Alert().render({ parentClasses }).el)
  }
}
