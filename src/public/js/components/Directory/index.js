/* eslint-env browser */
/* global $ */

import Component from '../Component'

export default class Directory extends Component {
  static load () {
    if (window.dirURI) return
    const description = 'Directory'
    const classes = ['far', 'fa-folder']
    const onClick = function () {
      window.location.href = window.location.pathname.replace(/[^/]+$/, '')
    }
    $('nav').component().addButton(description, classes, onClick)
  }
}
