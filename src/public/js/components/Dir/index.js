/* eslint-env browser */
/* global $ */

import styles from './styles'
import layout from './layout'
import Component from '../Component'
import DirFilters from '../DirFilters'
import moment from 'moment'

export default class Dir extends Component {
  constructor () {
    super(layout, styles)
  }
  async getDir () {
    const path = window.location.pathname
    try {
      const { entries } = await $.ajax({
        url: '/list',
        method: 'POST',
        data: JSON.stringify({ path }),
        contentType: 'application/json',
        processData: false
      })
      this.entries = entries
    } catch (err) {
      $('#container > .alert-container').component().addAlert({
          dismissable: true,
          content: 'Error getting dir, see console.'
      })
      throw err
    }
  }
  applyFilter (filter) {
    this.rows = filter(this.entries)
    this.render()
  }
  render () {
    super.render({ rows: this.rows, moment })
    return this
  }
  static async load () {
    if (!window.dirURI) return
    DirFilters.load()
    const dir = new Dir()
    await dir.getDir()
    dir.applyFilter((nodes) => nodes)
    $('#dir').replaceWith(dir.el)
  }
}

