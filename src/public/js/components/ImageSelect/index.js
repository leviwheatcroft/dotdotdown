/* eslint-env browser */
/* global $ */

import styles from './styles'
import layout from './layout'
import Component from '../Component'
import ImageUpload from '../ImageUpload'
import Alert from '../Alert'
import Uri from '../../Uri'

class ImageSelect extends Component {
  constructor () {
    super(layout, styles)
  }
  async getImages () {
    const path = new Uri(window.location.pathname)
    try {
      const images = await $.ajax({
        url: '/images',
        method: 'POST',
        data: JSON.stringify({ path: path.dir }),
        contentType: 'application/json',
        processData: false
      })
      this.images = images
      console.log(images)
    } catch (err) {
      $('.master-alert').component().addAlert({
        context: 'danger',
        content: 'Error getting images, see console.'
      })
      throw err
    }
  }
  render () {
    super.render({ images: this.images })
    this.el.on('hidden.bs.modal', () => this.el.modal('dispose'))
    Alert.load($('.alert-container', this.el))
    return this
  }
  clickSelect (event) {
    event.preventDefault()
    const codemirror = $('.editor').component().editor.codemirror
    const url = this.url
    const description = codemirror.getSelection() || url
    const markdown = `![${description}](${url})`
    codemirror.replaceSelection(markdown)
    this.el.modal('hide')
  }
  clickUpload () {
    this.el.modal('hide')
    new ImageUpload()
    .render()
    .el.appendTo($('body'))
    .modal('show')
  }
  clickImage (event) {
    $('img', this.el).removeClass('selected')
    this.url = event.target.getAttribute('src')
    $(event.target).addClass('selected')
  }
}

export async function imageSelect() {
  const imageSelect = new ImageSelect()
  await imageSelect.getImages()
  imageSelect
  .render()
  .el.appendTo($('body'))
  .modal('show')
}
