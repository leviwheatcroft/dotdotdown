/* eslint-env browser */
/* global $ */
import { imageSelect } from '../ImageSelect'
import './styles'
import SimpleMDE from 'simplemde'
// import 'simplemde/src/css/simplemde.css'

export default class Editor {
  constructor () {

  }
  render () {
    let parent = $('.editor')
    if (!parent.length) return
    parent.data('component', this)
    parent = parent[0]
    this.editor = new SimpleMDE({
      element: parent,
      autoDownloadFontAwesome: false,
      toolbar: [
        'bold',
        'italic',
        'strikethrough',
        'heading-1',
        'heading-2',
        'heading-3',
        'separator-1',
        'code',
        'quote',
        'unordered-list',
        'ordered-list',
        'link',
        {
            name: 'imageSelect',
            action: imageSelect,
            className: 'far fa-image',
            title: 'Insert Image'
        },
        'table',
        'horizontal-rule',
        'separator-2',
        'preview',
        'side-by-side',
        'fullscreen',
        'separator-3',
        'guide'
      ]
    })

  }
  static load () {
    const editor = new Editor()
    editor.render()
  }
}
