/* eslint-env browser */
/* global $ */

import styles from './styles'
import layout from './layout'
import Component from '../Component'
import upload from '../../upload'
import Alert from '../Alert'
import Uri from '../../Uri'

export default class ImageUpload extends Component {
  constructor () {
    super(layout, styles)
  }
  render () {
    super.render()
    this.el.on('hidden.bs.modal', () => this.el.remove())
    Alert.load($('.alert-container', this.el))
    return this
  }
  inputFile (event) {
    const path = new Uri(window.location.pathname)
    const file = event.target.files[0]
    if (!file) return
    const reader = new FileReader()
    reader.addEventListener('load', () => {
      $('img', this.el)
      .attr('src', reader.result)
      .removeClass('d-none') // show img
    }, false)
    reader.readAsDataURL(file)
    $('input[name=path]').val(`${path.dir}${file.name}`)
  }
  async clickUpload () {
    const codemirror = $('.editor').component().editor.codemirror
    const data = new FormData()
    data.append('file', $('input[name=file]')[0].files[0])
    data.append('path', $('input[name=path]').val())
    try {
      const result = await upload(data)
      const uri = new Uri(result.uri)
      const description = codemirror.getSelection() || uri.base
      const markdown = `![${description}](${result.uri})`
      codemirror.replaceSelection(markdown)
      this.el.modal('hide')
    } catch (err) {
      $('.alert-container', this.el).component().addAlert({
        dismissable: true,
        content: 'Error uploading image, see console'
      })
      throw err
    }
  }
}
