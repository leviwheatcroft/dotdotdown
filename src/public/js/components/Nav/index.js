/* eslint-env browser */
/* global $ */

import classNames from 'classnames'

import styles from './styles'
import layout from './layout'
import Component from '../Component'

import buttonLayout from './buttonLayout.pug'


export default class Nav extends Component {
  constructor () {
    super(layout, styles)
  }
  addButton (description, classes, onClick) {
    const locals = { description, classes, classNames }
    const button = $(buttonLayout(locals))
    button.on('click', onClick)
    $('ul', this.el).append(button)
  }
  toggle () {
    this.el.toggleClass('show')
  }
  static load() {
    $('nav#sidebar').replaceWith(new Nav().render().el)
  }
}
