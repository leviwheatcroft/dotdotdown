/* eslint-env browser */
/* global $ */
import classNames from 'classnames'

const renderUtils = {
  classNames
}

export default class Component {
  constructor (layout, styles) {
    Object.assign(this, { layout, styles })
    this.el = false
  }
  render (locals = {}) {
    const component = this
    Object.assign(
      locals,
      renderUtils,
      this.styles
    )
    const el = $(this.layout(locals))
    const events = [ 'click', 'input', 'submit', 'keypress' ]
    events.forEach((e) => {
      // convert some-event to SomeEvent
      const humps = e.replace(/(^\w|-\w)/g, (m) => m.slice(-1).toUpperCase())
      $(`[data-on-${e}]`, el).each(function () {
        const methodName = $(this).data(`on${humps}`)
        try {
          $(this).on(e, component[methodName].bind(component))
        } catch (err) {
          console.log(`'${methodName}' doesn't seem to exist on component`)
          console.log(this)
        }
      })
    })
    // store a reference to the component on the element
    el.data('component', this)
    // attach to dom
    if (this.el) this.el.replaceWith(el)
    // store element
    this.el = el
    return this
  }
}

$.fn.extend({
  component: function () {
    const component = $(this[0]).data('component')
    if (!component) {
      console.log($(this)[0])
      throw new Error('el doesnt have attached component')
    }
    else return component
  }
})
