/* eslint-env browser */
/* global $ */

export default function upload(data) {
  return $.ajax({
    type: 'POST',
    enctype: 'multipart/form-data',
    processData: false,
    contentType: false,
    cache: false,
    data,
    url: '/upload',
    timeout: 60000
  })
}
