import express from 'express'
import plugins from '../plugins'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-api')

const router = express.Router()

Object.keys(plugins).forEach((plugin) => {
  if (!plugins[plugin].router) return // ignore plugins with no router
  router.use(`/${plugin}`, plugins[plugin].router)
})

export default router
