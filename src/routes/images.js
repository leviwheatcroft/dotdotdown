import Path from '../Path'
import httpErrors from 'http-errors'
import {
  promises as fsPromises
} from 'fs'
const {
  readdir
} = fsPromises
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-images')

export default async function (req, res, next) {
  const path = Path.from(req.body.path)
  let dirEnts
  try {
    dirEnts = await readdir(path.asPath(), { withFileTypes: true })
    dbg(dirEnts)
  } catch (err) {
    if (err.code === 'ENOENT' || err.code === 'ENOTDIR') {
      let msg = `dir: req path doesn't exist or not a dir: ${req.body.path}`
      return next(httpErrors(400, msg, err))
    }
    let msg = 'not really sure what went wrong sorry'
    return next(httpErrors(500, msg, err))
  }
  const images = []
  while (dirEnts.length) {
    const dirEnt = dirEnts.shift()
    if (dirEnt.isDirectory()) continue
    const entryPath = path.child(dirEnt.name).relativeTo(path.asPath())
    if (
      /\.png$/i.test(entryPath.asPath()) ||
      /\.jpg$/i.test(entryPath.asPath()) ||
      /\.bmp$/i.test(entryPath.asPath()) ||
      /\.gif$/i.test(entryPath.asPath()) ||
      /\.svg$/i.test(entryPath.asPath())
    ) images.push(entryPath.asURI())
  }
  res.json(images)
}
