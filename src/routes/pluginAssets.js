import express from 'express'
import plugins from '../plugins'

const router = express.Router()

Object.keys(plugins).forEach((plugin) => {
  router.get(`/${plugin}.js`, (req, res) => {
    res.sendFile(plugins[plugin].componentPath)
  })
})

export default router
