import Path from '../Path'
import httpErrors from 'http-errors'
import {
  readdir
} from 'mz/fs'
import {
  statSync
} from 'fs'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-dir')

export default async function (req, res, next) {
  const path = Path.from(req.body.path)

  let entryNames

  try {
    entryNames = await readdir(path.asPath())
  } catch (err) {
    if (err.code === 'ENOENT' || err.code === 'ENOTDIR') {
      let msg = `dir: req path doesn't exist or not a dir: ${req.body.path}`
      return next(httpErrors(400, msg, err))
    }
    let msg = 'not really sure what went wrong sorry'
    return next(httpErrors(500, msg, err))
  }
  let entries = []
  while (entryNames.length) {
    const entryPath = path.child(entryNames.shift())
    const parsed = entryPath.parse()
    let stats
    try {
      stats = statSync(entryPath.asPath())
    } catch (err) {
      const msg = `can't stat that (permissions?): ${entryPath.asPath()}`
      return next(httpErrors(400, msg, err))
    }
    entries.push({
      url: entryPath.asURI(),
      name: parsed.base,
      isDirectory: stats.isDirectory(),
      isFile: stats.isFile(),
      isMarkdown: /\.md$/.test(entryPath.asPath()),
      isImage: /\.(gif|jpg|jpeg|png)$/i.test(entryPath.asPath()),
      base: parsed.base,
      modified: stats.mtimeMs
    })

  }
  res.json({ entries })
}
