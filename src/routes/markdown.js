import marked from 'marked'
import {
  readFile
} from 'mz/fs'
import Path from '../Path'
import httpErrors from 'http-errors'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-page')

export default async function (req, res, next) {
  const path = Path.from(req.path)
  const title = path.parse().name
  let markdown
  try {
    markdown = await readFile(path.asPath(), 'utf8')
  } catch (err) {
    const msg = `markdown file doesn't exist: ${path.asPath()}`
    return next(httpErrors(400, msg, err))
  }
  const contents = marked(markdown)

  res.render('markdown', { contents, markdown, path, title })
}
