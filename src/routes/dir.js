import Path from '../Path'
import {
  stat
} from 'mz/fs'
import httpErrors from 'http-errors'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-page')

export default async function (req, res, next) {
  const path = Path.from(req.path)
  const title = path.parse().name
  let stats
  try {
    stats = await stat(path.asPath())
  } catch (err) {
    dbg(err)
    if (err.code === 'ENOENT') return next()
    const msg = `untrapped error trying to stat: ${path.asPath()}`
    next(httpErrors(500, msg, err))
  }
  if (!stats.isDirectory()) return next()
  return res.render('dir', { path, title })
}
