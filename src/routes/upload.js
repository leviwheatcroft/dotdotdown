import {
  readdir
} from 'mz/fs'
// import {
//   parse
// } from 'path'
import Path from '../Path'
import httpErrors from 'http-errors'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-upload')

export default async function upload (req, res, next) {
  let path = Path.from(req.body.path)
  let entries
  try {
    entries = await readdir(path.parse().dir)
  } catch (err) {
    if (err.code === 'ENOENT') throw err
    if (err.code === 'ENOTDIR') throw err
  }
  while (entries.includes(path.parse().base)) path.increment()
  try {
    await req.files.file.mv(path.asPath())
  } catch (err) {
    const msg = `some error writing this path: ${path.asPath()}`
    return next(httpErrors(400, msg, err))
  }
  res.json({ uri: path.asURI() })
}
