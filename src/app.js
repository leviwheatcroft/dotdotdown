import 'babel-polyfill'

import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import debug from 'debug'
import moment from 'moment'
import fileUpload from 'express-fileupload'
// import expressHashWebpack from 'express-hash-webpack'
import config from 'config'
import httpErrors from 'http-errors'
import {
  createServer
} from 'net'
import {
  get
} from 'http'
import {
  dir,
  images,
  list,
  markdown,
  upload
} from './routes'
import api from './routes/api'
import pluginAssets from './routes/pluginAssets'
import plugins from './plugins'
import Path from './Path'
import {
  statSync
} from 'fs'


const dbg = debug('ddd-app')


// ensure root directory is accessible
try {
  statSync(Path.from('/').asPath())
  dbg(`root is: ${config.get('root')}`)
} catch (err) {
  dbg(`some problem accessing ${config.get('root')}, maybe check that`)
  throw new Error(`can't access root: ${config.get('root')}`)
}


let app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// locals
app.locals.moment = moment
app.locals.js = Object.keys(plugins).map((plugin) =>  `/public/${plugin}.js`)
app.locals.js.unshift('/public/main.js')


// middleware
app.use(logger('dev'))
// app.use(expressHashWebpack({
//   hashPath: config.get('path.public'),
//   hashFileName: './hash.txt',
//   hashTemplate: '.[value]',
//   assetTemplate: '/public[path][name][hash][ext]',
//   cache: false
// }))
// app.locals.js.push(app.locals.assetPath('main.js'))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(fileUpload())
app.use(cookieParser())

app.use(
  '/public',
  express.static(config.get('path.public')),
  pluginAssets
)
app.use('/ddd', api)

app.post('/images', images)
app.post('/upload', upload)
app.post('/list', list)
app.get(/\.md$/, markdown)
// static *must* be after markdown, otherwise it will serve markdown files
app.use(express.static(config.get('root')))
app.use(dir)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(httpErrors(404, `${req.url} not found`))
});

// error handler
// this is an error handling middleware. It *needs* all 4 args
// eslint-disable-next-line no-unused-vars
app.use(function(err, req, res, _) {
  res.locals.error = err
  res.status(err.status || 500)
  if (req.get('content-type') === 'application/json') res.json({ err })
  else res.render('error')
})

function getPort(port = 3000) {
  return new Promise((resolve, reject) => {
    const server = createServer()
    server.on('error', (err) => {
      if (err.code !== 'EADDRINUSE') return reject(err)
      server.listen(++port)
    })
    server.on('listening', () => server.close(() => resolve(port)))
    server.listen(port)
  })
}

if (process.env.NODE_ENV === 'dev') {
  dbg('dev mode')
  app.listen(3000, () => {
    dbg('app (re)started, request browser reload')
    get('http://localhost:3001/__browser_sync__?method=reload')
    .on('error', () => console.log('browser-sync not listening'))
  })
} else {
  getPort()
  .then((port) => {
    app.listen(port, () => dbg(`dot dot down listening on port ${port}`))
  })
}
