import config from 'config'
import {
  join
} from 'path'
import Path from './Path'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-plugins')

const pluginNames = config.get('plugins')

const plugins = {}

const dotdotdown = {
  config,
  Path
}

pluginNames.forEach((pluginName) => {
  const shortName = pluginName.replace(/^dotdotdown-/, '')
  const getRouter = require(pluginName).default
  plugins[shortName] = {
    router: getRouter ? getRouter(dotdotdown) : false,
    componentPath: require.resolve(join(pluginName, 'dist', 'component.js'))
  }
})

export default plugins
